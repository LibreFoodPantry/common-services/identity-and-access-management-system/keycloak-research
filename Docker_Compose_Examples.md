﻿# Keycloak Docker Compose Files ( .yml )

Here are multiple examples of Docker Compose Files for Keycloak with different configurations.



#  Keycloak Docker Compose Examples

Examples for using Keycloak with Docker Compose. 
Github link can be found "https://github.com/keycloak/keycloak-containers/tree/main/docker-compose-examples"
Originally posted by "[ERRECabrera](https://github.com/ERRECabrera)"

##  Keycloak and PostgreSQL

The `keycloak-postgres.yml` template creates a volume for PostgreSQL and starts Keycloak connected to a PostgreSQL instance.

Run the example with the following command:

docker-compose -f keycloak-postgres.yml up

Open http://localhost:8080/auth and login as user 'admin' with password 'Pa55w0rd'.

Note - If you run the example twice without removing the persisted volume there will be a warning 'user with username exists'. You can ignore this warning.
```    
version: '3'

volumes:
  postgres_data:
      driver: local

services:
  postgres:
      image: postgres
      volumes:
        - postgres_data:/var/lib/postgresql/data
      environment:
        POSTGRES_DB: keycloak
        POSTGRES_USER: keycloak
        POSTGRES_PASSWORD: password
  keycloak:
      image: quay.io/keycloak/keycloak:legacy
      environment:
        DB_VENDOR: POSTGRES
        DB_ADDR: postgres
        DB_DATABASE: keycloak
        DB_USER: keycloak
        DB_SCHEMA: public
        DB_PASSWORD: password
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: Pa55w0rd
        # Uncomment the line below if you want to specify JDBC parameters. The parameter below is just an example, and it shouldn't be used in production without knowledge. It is highly recommended that you read the PostgreSQL JDBC driver documentation in order to use it.
        #JDBC_PARAMS: "ssl=true"
      ports:
        - 8080:8080
      depends_on:
        - postgres
```

##  Keycloak and PostgreSQL with JDBC_PING

Similarly to other templates, the `keycloak-postgres-jdbc-ping.yml` template creates a volume for PostgreSQL and boots up Keycloak connected to it. Traefik is used like load balancer to redirect all request to diferent container instances. The most important change is the JGroups Discovery protocol that is used in this configuration, which is `JDBC_PING`. `JDBC_PING` reuses the same database instance as all Keycloak servers in the cluster.

Run the example with the following command:

docker-compose -f keycloak-postgres-jdbc-ping.yml up --scale keycloak=2

Once the cluster is started, open http://localhost:8080/auth and login as user 'admin' with password 'Pa55w0rd'.

Note - Sometimes, it is necessary to adjust the `JDBC_PING` initialization SQL (see the [manual](http://jgroups.org/manual/#_jdbc_ping)). In such cases, make sure you specify proper `initialize_sql` string into `JGROUPS_DISCOVERY_PROPERTIES` property. For more information, please refer to [JGroups codebase](https://github.com/belaban/JGroups/blob/master/src/org/jgroups/protocols/JDBC_PING.java).

```   
version: '3'

volumes:
  postgres_data:
    driver: local

services:
  postgres:
    image: 'postgres:alpine'
    volumes:
      - ./postgres:/var/lib/postgresql/data
    restart: 'always'
    # ports:
    #   - 5432:5432
    environment:
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD: password
      POSTGRES_DB: keycloak
      POSTGRES_HOST: postgres

  traefik:
    image: library/traefik:alpine
    container_name: traefik
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    command: >
      --logLevel=ERROR
      --api.dashboard
      --docker
      --entrypoints="Name:http Address::80"
      --defaultentrypoints="http"
    ports:
      - 80:80
      - 3000:8080

  keycloak:
    image: jboss/keycloak:legacy
    environment:
      DB_VENDOR: postgres
      DB_ADDR: postgres
      DB_PORT: 5432
      DB_DATABASE: keycloak
      DB_USER: keycloak
      DB_PASSWORD: password
      KEYCLOAK_USER: admin
      KEYCLOAK_PASSWORD: Pa55w0rd
      # KEYCLOAK_LOGLEVEL: DEBUG
      JGROUPS_DISCOVERY_PROTOCOL: JDBC_PING
      JGROUPS_DISCOVERY_PROPERTIES: datasource_jndi_name=java:jboss/datasources/KeycloakDS,info_writer_sleep_time=500,initialize_sql="CREATE TABLE IF NOT EXISTS JGROUPSPING ( own_addr varchar(200) NOT NULL, cluster_name varchar(200) NOT NULL, created timestamp default current_timestamp, ping_data BYTEA, constraint PK_JGROUPSPING PRIMARY KEY (own_addr, cluster_name))"
    depends_on:
      - postgres
    labels:
      traefik.enable: true
      traefik.port: 8080
      traefik.protocol: http
      traefik.frontend.rule: Host:localhost
      traefik.frontend.passHostHeader: true
      # traefik.backend.loadbalancer.stickiness: true  
```

##  Keycloak and MySQL

The `keycloak-mysql.yml` template creates a volume for MySQL and starts Keycloak connected to a MySQL instance.

Run the example with the following command:

docker-compose -f keycloak-mysql.yml up

Note - This example is not currently working as MySQL is not ready to receive connections when Keycloak is started.

```  
version: '3'

volumes:
  mysql_data:
      driver: local

services:
  mysql:
      image: mysql:5.7
      volumes:
        - mysql_data:/var/lib/mysql
      environment:
        MYSQL_ROOT_PASSWORD: root
        MYSQL_DATABASE: keycloak
        MYSQL_USER: keycloak
        MYSQL_PASSWORD: password
  keycloak:
      image: quay.io/keycloak/keycloak:legacy
      environment:
        DB_VENDOR: MYSQL
        DB_ADDR: mysql
        DB_DATABASE: keycloak
        DB_USER: keycloak
        DB_PASSWORD: password
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: Pa55w0rd
        # Uncomment the line below if you want to specify JDBC parameters. The parameter below is just an example, and it shouldn't be used in production without knowledge. It is highly recommended that you read the MySQL JDBC driver documentation in order to use it.
        #JDBC_PARAMS: "connectTimeout=30000"
      ports:
        - 8080:8080
      depends_on:
        - mysql
```

##  Keycloak and MariaDB with JDBC_PING

Similarly to other templates, the `keycloak-mariadb-jdbc-ping.yml` template creates a volume for MariaDB and boots up Keycloak connected to it. The most important change is the JGroups Discovery protocol that is used in this configuration, which is `JDBC_PING`. `JDBC_PING` reuses the same database instance as all Keycloak servers in the cluster.

Run the example with the following command:

docker-compose -f keycloak-mariadb-jdbc-ping.yml up --scale keycloak=2

Once the cluster is started, use `docker ps` and `docker inspect` commands to obtain one of the Keycloak server IPs. Then open http://<ip>:8080/auth and login as user 'admin' with password 'Pa55w0rd'.

Note - Sometimes, it is necessary to adjust the `JDBC_PING` initialization SQL (see the [manual](http://jgroups.org/manual/#_jdbc_ping)). In such cases, make sure you specify proper `initialize_sql` string into `JGROUPS_DISCOVERY_PROPERTIES` property. For more information, please refer to [JGroups codebase](https://github.com/belaban/JGroups/blob/master/src/org/jgroups/protocols/JDBC_PING.java).

```
version: '3'

volumes:
  mysql_data:
      driver: local

services:
  mariadb:
      image: mariadb
      volumes:
        - mysql_data:/var/lib/mysql
      environment:
        MYSQL_ROOT_PASSWORD: root
        MYSQL_DATABASE: keycloak
        MYSQL_USER: keycloak
        MYSQL_PASSWORD: password
      # Copy-pasted from https://github.com/docker-library/mariadb/issues/94
      healthcheck:
        test: ["CMD", "mysqladmin", "ping", "--silent"]
  keycloak:
      image: quay.io/keycloak/keycloak:legacy
      environment:
        DB_VENDOR: mariadb
        DB_ADDR: mariadb
        DB_DATABASE: keycloak
        DB_USER: keycloak
        DB_PASSWORD: password
        KEYCLOAK_USER: admin
        KEYCLOAK_PASSWORD: Pa55w0rd
        JGROUPS_DISCOVERY_PROTOCOL: JDBC_PING
      depends_on:
        - mariadb
```

##  Keycloak and Microsoft SQL Server

The `keycloak-mssql.yml` template creates a volume for Microsoft SQL Server and starts Keycloak connected to a Microsoft SQL Server instance.

Run the example with the following command:

docker-compose -f keycloak-mssql.yml up

Note - This example uses an additional container to create the keycloak database prior to loading the keycloak application. In addition, the keycloak container can be rebuilt using

docker-compose -f ./docker-compose-examples/keycloak-mssql.yml build

##  Troubleshooting

###  User with username exists

If you get a error `Failed to add user 'admin' to realm 'master': user with username exists` this is most likely because

you've already ran the example, but not deleted the persisted volume for the database. In this case the admin user already

exists. You can ignore this warning or delete the volume before trying again.

```
version: "3.2"

services:

mssql:

image: mcr.microsoft.com/mssql/server

ports:

- "1433:1433"

environment:

- ACCEPT_EULA=Y

- SA_PASSWORD=Password!23

- MSSQL_PID=Developer

mssqlscripts:

image: mcr.microsoft.com/mssql-tools

depends_on:

- mssql

command: /bin/bash -c 'until /opt/mssql-tools/bin/sqlcmd -S mssql -U sa -P "Password!23" -Q "create database Keycloak"; do sleep 5; done'

keycloak:

image: quay.io/keycloak/keycloak:legacy

depends_on:

- mssql

- mssqlscripts

ports:

- "8080:8080"

environment:

- KEYCLOAK_USER=admin

- KEYCLOAK_PASSWORD=admin

- DB_VENDOR=mssql

- DB_USER=sa

- DB_PASSWORD=Password!23

- DB_ADDR=mssql

- DB_DATABASE=Keycloak
```
